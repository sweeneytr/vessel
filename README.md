# vessel

A networked terminal simulation of a vessel.

# contributing

Wotkflow follows standards [gitflow model](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).